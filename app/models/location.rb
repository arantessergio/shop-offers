class Location < ActiveRecord::Base
  belongs_to :province
  belongs_to :city
  belongs_to :district
  has_many :shop
end
