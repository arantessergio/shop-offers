class Shop < ActiveRecord::Base
  has_many :establishments
  belongs_to :location
end
