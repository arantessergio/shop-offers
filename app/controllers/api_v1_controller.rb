class ApiV1Controller < ApplicationController
  skip_before_filter :verify_authenticity_token

  def provinces
    p = Province.all
    render :json => {msg: p}
  end

  def cities
    c = City.where(:province_id => params[:province_id])
    render :json => {msg: c}
  end

  def districts
    d = District.where(:city_id => params[:city_id])
    render :json => {msg: d}
  end

  def shops
    s = Shop.where(:location_id => params[:location_id])
    render :json => {msg: s}
  end

  def establishments
    e = Establishment.where(:shop_id => params[:shop_id])
    render :json => {msg: e}
  end

end
