class AddColunmsToShop < ActiveRecord::Migration
  def change
    add_column :shops, :descricao, :string
    add_column :shops, :cnpj, :string
    add_column :shops, :inscricao_estadual, :string
  end
end
