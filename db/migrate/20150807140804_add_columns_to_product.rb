class AddColumnsToProduct < ActiveRecord::Migration
  def change
    add_column :products, :name, :string
    add_column :products, :establishment_id, :integer
    add_column :products, :price, :decimal
    add_column :products, :deduction, :decimal
    add_column :products, :is_offer, :boolean
  end
end
