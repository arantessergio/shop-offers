class AddColumnToProvince < ActiveRecord::Migration
  def change
    add_column :provinces, :uf, :string
    add_column :provinces, :name, :string
  end
end
