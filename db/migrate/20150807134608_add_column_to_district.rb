class AddColumnToDistrict < ActiveRecord::Migration
  def change
    add_column :districts, :city_id, :integer
    add_column :districts, :name, :string
  end
end
