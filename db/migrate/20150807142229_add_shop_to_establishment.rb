class AddShopToEstablishment < ActiveRecord::Migration
  def change
    add_column :establishments, :shop_id, :integer
    add_column :establishments, :name, :string
    add_column :establishments, :cnpj, :string
    add_column :establishments, :ie, :string
  end
end
