class AddColumnToCity < ActiveRecord::Migration
  def change
    add_column :cities, :province_id, :integer
    add_column :cities, :name, :string
    add_column :cities, :ibge, :string
  end
end
