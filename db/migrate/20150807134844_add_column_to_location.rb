class AddColumnToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :province_id, :integer
    add_column :locations, :city_id, :integer
    add_column :locations, :district_id, :integer
  end
end
